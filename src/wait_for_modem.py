""" Delays the start of gammu-smsd until modem fully initiated after reboot"""

import serial.tools.list_ports
import time
import re
import sys
import os
import gammu
import configparser

config = configparser.RawConfigParser()
config.read('/etc/gammu-smsdrc')

simPIN = config.get('smsd', 'PIN')
print("PIN",simPIN)
smscNumber =   config.get('smsd', 'SMSC')
print("SMSC",smscNumber)

def initModem():
  # Create state machine object
  sm = gammu.StateMachine()

  # Read config file
  sm.ReadConfig(Section=0,Filename="/etc/gammurc")
  #sm.ReadConfig(Section=1,Filename="/etc/gammurc")

  #conf0 = sm.GetConfig(0)
  #print('config 0:')
  #print(conf0)

  #conf1 = sm.GetConfig(1)
  #print('config 1:')
  #print(conf)

  # Connect to phone
  sm.Init()

  sec = sm.GetSecurityStatus()
  print("pre-PIN: security status", sec)

  if sec == "PIN": sm.EnterSecurityCode("PIN",simPIN)

  sec = sm.GetSecurityStatus()
  print("post-PIN: security status", sec)
  time.sleep(1)

  print("getting SMSC")
  attempts = 100
  for attempt in range(attempts):
    print("attempt",attempt)
    try:
      #sm.SetSMSC(smsc)
      smsc = sm.GetSMSC()
      print("getSMSC succes. Number %s" % smsc["Number"])
      break
    except:
      print("getSMSC failure")
      time.sleep(1)

  time.sleep(1)

  smsc["Number"] = smscNumber
  sm.SetSMSC(smsc)
  smsc = sm.GetSMSC()
  print('smsc Mumber: %s' % smsc["Number"])
  time.sleep(2)

def waitForModem(searchString,expectedMatches,maxAttempts=20,sleepTime=5):
  attempts = 0
  while attempts < maxAttempts:
    matches = 0
    attempts = attempts +1
    print("Attempt:",attempts)
    ports = serial.tools.list_ports.comports()
    print("  ports found:", len(ports))
    for port, desc, hwid in sorted(ports):
      match = re.search(searchString,desc)
      if match:
        matches = matches + 1 
        result = "MODEM !"
      else: result = "other" 
      print("    {}: {} [{}] {}".format(port, desc, hwid, result))
    print("    matches: {} expectedMatches {}".format(matches,expectedMatches))
    if matches == expectedMatches: return True
    time.sleep(sleepTime)
  return False


result = waitForModem("ZTE", 3, maxAttempts=1)
if result:
  # Already in proper state; further init leave
  initModem() 
  sys.exit(0)
else: 
  result = waitForModem("ZTE", 1)
  if result:
    # If matches is 1 force to go into modemmode
    os.system("echo '19d2 0016' > /sys/bus/usb-serial/drivers/option1/new_id")
    result = waitForModem("ZTE", 3)
    if result:
      # now in proper state; further init leave
      initModem() 
      sys.exit(0)
  else:
    print("No ZTE modem found")
    sys.exit(1) # modem not found in time
