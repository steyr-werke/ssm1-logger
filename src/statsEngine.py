#!/usr/bin/env python3
"""
Statistics component
"""
import csv
import logging
import os
import sys
import statistics
import time

import main

def calculateMedian(dataList_float):
    s = statistics.median(dataList_float)
    #logging.info(f"Median: {s}")
    return s


def calculateMean(dataList_float):
    s = statistics.mean(dataList_float)
    #logging.info(f"Mean: {s}")
    return s

class multiStats(object):
    def __init__(self, meanVal, medVal, minVal, maxVal):
        self.meanValue   = meanVal
        self.medianValue = medVal
        self.minValue    = minVal
        self.maxValue    = maxVal

def calculateStats(dataList_float):
    medVal = statistics.median(dataList_float)
    maxVal = max(dataList_float)
    minVal = min(dataList_float)
    meanVal = statistics.mean(dataList_float)
    s = multiStats(meanVal, medVal, minVal,  maxVal)
    #logging.info(f"Stats: {s}")
    return s

def getLastValueOfList(dataList):
    """Return last value on the end of the list"""
    return dataList[len(dataList)-1]


def calculateMeanFromTo(dataLogPath, numDataEntriesToEvaluate, hint):
    if not os.path.exists(dataLogPath):
        return 0

    l = []
    result = 0
    with open(dataLogPath, 'r') as f:
        fieldnames = ['date', 'time', 'counter', 'value']
        dataLog = csv.DictReader(f, fieldnames=fieldnames, delimiter=';')

        for row in dataLog:
            # logging.info(dataLogPath, row)
            try:
                d = float(row['value'])
            except:
                logging.info(f"ERROR reading csv: {dataLogPath}")
                raise
            l.append(d)

            if len(l) > numDataEntriesToEvaluate:
                l.pop(0)  # remove the oldest entry

    if len(l) < 1:
        # logging.info("data too short")
        return 0

    #_exportDataToFile(l, hint)

    #logging.info(l)

    #result = statistics.median(l)
    result = statistics.mean(l)
    return result


# def _exportDataToFile(data, hint):
#     with open(f"log/dataOnly_{hint}_for_validation.csv", 'a') as f: # FIXME does not work as path is relative
#         writer = csv.writer(f, delimiter=';')
#         for i in range(len(data)-1):
#             writer.writerow([data[i]])


def _getStats_MidrangeByNumberOfDatasetsInSeconds(dataEntries, numberOfDatasets):  # Mittelwert
    """Mittelwert über Anzahl von Datasets (Eintrag jede Sekunde)"""
    if numberOfDatasets > len(dataEntries): # At startup, list is empty
        # numberOfDatasets = len(dataEntries)
        return 0  # not possible, range too big

    val = 0
    for i in range(numberOfDatasets):
        val = val + float(dataEntries[i])

    m = val/numberOfDatasets
    logging.info("Midrange (%d/%d): %s" % (numberOfDatasets, len(dataEntries), m))
    return m



if __name__ == "__main__":
    print("Use main.py to start application")
