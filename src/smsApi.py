#!/usr/bin/env python3
"""
Sms public API, makes sure permissions are implemented.

Case insensitive.
"""

#
# ADMIN API: commands and aliases
#
adminCommands = {
    "ADD_USER": [ 'addUser', 'Tx' ],
    "DELETE_USER": [ 'deleteUser', 'delUser' ],
    "SET_USER_ROLE": [ 'setUserRole', 'setRole' ],
    "GET_USERS": ['getUsers', 'getAllUsers', 'users'],
    "GET_USAGE": ['getUsage']
}

#
# USER API: commands and aliases
#
userCommands = {
    "GET_VALUE": [ 'getValue', 'W' ],
    "GET_ALARM_VALUE": [ 'getAlarmValue'],
    "SET_ALARM_VALUE": [ 'setAlarmValue', 'setValue', 'S' ],
    "GET_ALARM_INTERVAL": [ 'getAlarmInterval' ],
    "SET_ALARM_INTERVAL": [ 'setAlarmInterval', 'setInterval', 'I' ],
    "HELP": ['help', 'H']
}



if __name__ == "__main__":
    print("Use main.py to start application")
