#!/usr/bin/env python3
"""
Function tests. TODO they throw, but now, the exception is caught and could be overseen.
"""
# import datetime
# import logging
import os
# import shutil
# import sys
import time
# import traceback

import config
# import dataStore
# import statsEngine
# import ruleEngine
import sms
import main


def runTests():
    # delete old config
    # config._writeConfigToFileAsDict({})
    # os.remove(main.CONFIG_PATH)

    # TEST config
    print(config.getWatchdogPhone())
    print(config.getAdminPhones())

    # test_config_user()
    # test_config_alarmValue()
    # test_config_alarmInterval()
    test_sms()


def test_sms():
    sms._splitIncomingCommand("addUser 1234567")
    sms._splitIncomingCommand("HElp")
    try:
        sms._splitIncomingCommand("notacommand")
    except:
        print("PASS successfully failed at wrong SMS command")
        pass
    sms._splitIncomingCommand("deleteUser 123456")



def test_config_user():
    print("[TEST] TEST CONFIG: add and delete users")
    config.addUser("TESTNR", "0043650ADMIN1")
    assert "TESTNR" == config.getUserPhone("TESTNR")
    print(str(len(config.getUsersPermissive("0043650ADMIN1"))) + " users found.")
    time.sleep(1)
    config.deleteUser("TESTNR", "0043650ADMIN1")
    assert "TESTNR" != config.getUserPhone("TESTNR")
    print(str(len(config.getUsersPermissive("0043650ADMIN1"))) + " users found.")


def test_config_alarmInterval():
    print("[TEST] TEST CONFIG: get and set alarm interval")
    config.addUser("TESTUSER", "0043650ADMIN1")
    assert "TESTUSER" == config.getUserPhone("TESTUSER")

    config.setAlarmInterval_minutes("TESTUSER", 61)
    assert 61 == config.getAlarmInterval_minutes("TESTUSER")

    config.setAlarmInterval_minutes("TESTUSER", "62")
    assert 62 == config.getAlarmInterval_minutes("TESTUSER")

    config.setAlarmInterval_minutes("TESTUSER", "NON_INTEGER_VALUE")
    assert 62 == config.getAlarmInterval_minutes("TESTUSER")

    config.setAlarmInterval_minutes("TESTUSER_NOT_EXISTING", "63")
    assert "" == config.getAlarmInterval_minutes("TESTUSER_NOT_EXISTING")

    config.setAlarmInterval_minutes("TESTUSER_NOT_EXISTING", "NON_INTEGER_VALUE")
    assert "" == config.getAlarmInterval_minutes("TESTUSER_NOT_EXISTING")

    config.deleteUser("TESTUSER", "0043650ADMIN1")
    assert "TESTUSER" != config.getUserPhone("TESTUSER")


def test_config_alarmValue():
    print("[TEST] TEST CONFIG: get and set alarm value")
    config.addUser("TESTUSER", "0043650ADMIN1")
    assert "TESTUSER" == config.getUserPhone("TESTUSER")

    config.setAlarmValue("TESTUSER", "0.3")
    assert 0.3 == config.getAlarmValue("TESTUSER")

    config.setAlarmValue("TESTUSER", "0,3")
    assert 0.3 == config.getAlarmValue("TESTUSER")

    config.setAlarmValue("TESTUSER", "0.04")
    assert 0.04 == config.getAlarmValue("TESTUSER")

    config.setAlarmValue("TESTUSER", "not_a_number")
    assert 0.04 == config.getAlarmValue("TESTUSER")

    config.deleteUser("TESTUSER", "0043650ADMIN1")
    assert "TESTUSER" != config.getUserPhone("TESTUSER")


def test_config_watchdogApi():
    print("[TEST] TESTING WATCHDOG API")
    config.getWatchdogPhone() #TODO finalize test
    raise ValueError


if __name__ == "__main__":
    print("Use main.py to start application")


