#!/usr/bin/env python3
"""
Integration tests.
"""
# import datetime
import logging
import os
import shutil
import sys
import time
# import traceback
import unittest
import pathlib

# import config
# import dataStore
# import statsEngine
# import ruleEngine
import sms
import main


testData = [
    {
        'testDesc': 'TEST help with correct number',
        'inFile': 'IN20230110_231120_00_+436502090815_00_help.txt',
        'inCommand': 'help\n',
        'outFile': 'OUTSMS_+436502090815_00.txt',
        'outResult': sms.THE_HELP_TEXT
    },
    # 'IN20230110_231120_00_00436502090815_00_help.txt', # test help with wrong number
    # 'IN20230110_231120_00_+436502090815_00.txt' # test invalid command
]
# fileNamePatterns = [
#     'IN20230110_231120_00_+436502090815_00_help.txt', # test help with correct number
#     'IN20230110_231120_00_00436502090815_00_help.txt', # test help with wrong number
#     'IN20230110_231120_00_+436502090815_00.txt' # test invalid command
# ]


def runTests():
    test_smsFileMailbox()


def test_smsFileMailbox():
    _cleanupSmsPaths()

    try:
        for i in range(0,len(testData)):
            testDataWithInFileAndOutFile = testData[i]
            run_sms_file_write_and_read_test(testDataWithInFileAndOutFile)
    except:
        _cleanupSmsPaths()
        raise


def run_sms_file_write_and_read_test(testData):
    """"""
    print(f"TEST START: {testData['testDesc']}")
    _createSmsInboxFileWithContent(testData['inFile'], testData['inCommand'])
    time.sleep(5) #wait for the application to create the outbox file
    res = _readSmsOutboxFileContent(testData['outFile'])
    _tearDown(testData['inFile'], testData['outFile'])

    if _validateTestResult(res, testData['outResult']):
        print(f"TEST PASS: {testData['testDesc']}")
    else:
        print(f"TEST FAIL: {testData['testDesc']}")
        raise ValueError()


def _validateTestResult(res, res_reference):
    return res.strip().startswith(res_reference.strip())


def _createSmsInboxFileWithContent(fileName, msg):
    # create inbox file
    with open(main.SMS_INBOX_PATH + fileName, 'w') as f:
        f.write(msg)


def _readSmsOutboxFileContent(fileName):
    with open(main.SMS_OUTBOX_PATH + fileName, 'r') as f:
        c = f.read()
        logging.info("Outgoing SMS:", c)
        return c.strip()


def _tearDown(inFilePath, outFilePath):
    try:
        os.remove(main.SMS_INBOX_PATH + inFilePath)
    except:
        pass
    try:
        os.remove(main.SMS_OUTBOX_PATH + outFilePath)
    except:
        pass


def _cleanupSmsPaths():
    for f in os.listdir(main.SMS_INBOX_PATH):
        os.remove(main.SMS_INBOX_PATH + f)
    for f in os.listdir(main.SMS_OUTBOX_PATH):
        os.remove(main.SMS_OUTBOX_PATH + f)


# def test_config_user():
#     print("[TEST] TEST CONFIG: add and delete users")
#     config.addUser("TESTNR", "0043650ADMIN1")
#     assert "TESTNR" == config.getUserPhone("TESTNR")



if __name__ == "__main__":
    result = runTests()

    sys.exit(result)


