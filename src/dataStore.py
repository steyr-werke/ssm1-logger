#!/usr/bin/env python3
"""
Data logger component
"""
import csv
import logging
import os
import sys
import time
import datetime
import re
import random
import serial
import paho.mqtt.client as mqtt

import main


#
# Begin MQTT
#
def on_connect(client, userdata, flags, rc):
    """Starting subscription on connect. 
        Subscribing in on_connect() means that if we lose the
        connection and reconnect then subscriptions will be renewed, 
        retained topics resent.
    """
    if rc == 0:
        client.connected_flag = True  # set flag
        logging.info("connected OK Returned code=%d", rc)

        client.subscribe("stw/dosimeter/request/TODO")

        logging.debug("Ei am konnektet.")
    else:
        logging.warning("Bad connection Returned code=%d", rc)


def on_message(client, userdata, data):  # One encodes strings, and one decodes bytes.
    topic = data.topic
    msg = data.payload
    logging.debug(topic, msg)



client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.will_set("stw/dosimeter/lastwill", "Dosimeter SSM1-Logger sagt gut bei.", qos=1, retain=False)
client.connected_flag = False
while not client.connected_flag:  # connect at system boot and retry util networking is "up"
    try:
        client.connect("127.0.0.1", 1883, 60)
        client.connected_flag = True
        client.publish("stw/dosimeter/welcome", " sagt Hallo!")

    except Exception as ex:
        logging.warn("MQTT Connect failed! ")
        logging.warn(ex)
        time.sleep(5)


# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
#client.loop_forever()
# NEW: Non blocking call loop_start to be able to start cron in the loop
client.loop_start()

#
# End MQTT
#

def getDataEntry():
    """ Connect to serial, get single measurement and return.

        "0001 0.03 micro_Sievert pro Stunde"
    """
    ser = serial.Serial(port=main.SERIAL_PORT, baudrate=main.SERIAL_BAUD)
    ser.timeout = 2

    if ser.is_open:
        readResult = ser.readline().strip().decode()

    #time.sleep(1) #time is controlled by serial device
    return readResult


def getDataString_mock_expected_data():
    time.sleep(1)  #should be 1; set to 0.001 or 0.000001 for testing fast
    return("0001 %s micro_Sievert pro Stunde %s" % (random.randrange(20, 39)/1000, datetime.datetime.now()))


def getDataString_mockUnexpectedData(counter):
    currentDataString = _mock_unexpected_data(counter)
    time.sleep(1)  #should be 1; set to 0.001 or 0.000001 for testing fast
    return currentDataString


def _mock_unexpected_data(counter):
    """Create example data every second

    Create random possible strings like:
        - 0001 %s micro_Sievert
        - 9001 %s milli_Sievert
        - 12345 %s xxx
    """
    randVal = random.randrange(20, 39)/1000

    testStringFromSerial = [
        "empty_strings_incoming",
        "",
        "\n",
        "\r\n",
        f"0001 0.03{counter} micro_Sievert pro Stunde",
        f"9999 0.03{counter} micro_Sievert pro Stunde",
        f"99991 0.03{counter} micro_Sievert pro Stunde",
        f"9001 0.03{counter} milli_Sievert pro Stunde",
        f"9001 0.03{counter} mili_Sievert pro Stunde",
        f"12345 0.03{counter} micro_Sievert pro Stunde",
        f"12345 0.03{counter} xxx"
    ]

    # Extraordinary test cases come first
    if counter < len(testStringFromSerial):
        result = testStringFromSerial[counter]

    else:
        result = "0001 %s micro_Sievert pro Stunde" % randVal

    return result


def interpretDataString(d: str):
    """Stripe expected input data
        from: 0001 0.03 micro_Sievert pro Stunde 
        to  : 0.032

        If that conversion fails, raise exception to notify admins.
    """
    if not _unitIsInMicroSievert(d):
        raise ValueError("Unit is not in microSievert: %s" % d)

    mo1 = re.search(r"\d+ (\d+\.\d+) micro_Sievert pro Stunde.*", d)
    
    try:
        dataResult = mo1.group(1)
        return dataResult
    except AttributeError as UnitIsNotInMicroSievertException:
        raise ValueError("Value not expected: %s" % UnitIsNotInMicroSievertException)


def _unitIsInMicroSievert(line):
    if line.find("micro_Sievert pro Stunde") > -1:
        return True
    return False


def printLogEveryMinute(dataLog, minuteMeanValue, counter):
    """Print log message every minute"""
    if counter % 60 == 0:
        uptime_minutes = counter / 60
        uptime_hours = round(uptime_minutes/60, 1)
        l = f"Counter:{counter} Uptime_minutes:{uptime_minutes} Uptime_hours:{uptime_hours} logging mean value over last minute {minuteMeanValue} to dataLog (len: {len(dataLog)})"
        logging.debug(l)
        return l

def appendEveryMinute(dataLog, minuteMeanValue, counter):
    """Update data log in memory every minute"""
    if counter % 60 == 0:
        #logging.info(f"Minute passed... logging mean value over last minute {minuteMeanValue} to dataLog (len: {len(dataLog)}) ")
        date_isoformat, time_isoformat = main.getCurrentTimestamp()
        dataLog.append((date_isoformat, time_isoformat, counter, minuteMeanValue))
    return dataLog


def publishEveryMinute(dataLog, minuteStats, counter):
    """Publish to MQTT every minute"""
    if counter % 60 == 0:
        #logging.info(f"Minute passed... logging mean value over last minute {minuteMeanValue} to dataLog (len: {len(dataLog)}) ")
        topic = "stw/dosimeter/minuteMeanValue"
        msg = minuteStats.meanValue
        topic2 = "stw/dosimeter/minuteMedianValue"
        msg2 = minuteStats.medianValue
        topic3 = "stw/dosimeter/minuteMinValue"
        msg3 = minuteStats.minValue
        topic4 = "stw/dosimeter/minuteMaxValue"
        msg4 = minuteStats.maxValue
        try:
            client.publish(topic, msg)
            client.publish(topic2, msg2)
            client.publish(topic3, msg3)
            client.publish(topic4, msg4)
        except: 
            pass

    return dataLog


def saveToDiskAndCleanupEveryHour(csvPath, dataLog, counter):
    """Persist data log on disk every hour, if successfully saved, return empty list."""
    if not os.path.exists(csvPath):
        with open(csvPath, 'w') as csvData:
            csvData.write("") # must be empty csv
    if len(dataLog) < 60:
        return dataLog
    if counter % 3600 == 0:
        logging.info(f"Hour passed... write log to {csvPath} ✅")
        with open(csvPath, 'a', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=';')
            for i in range(len(dataLog)-60, len(dataLog)):
                date, time, counter, value = dataLog[i]
                writer.writerow([date, time, counter, value])
        return [] # delete data from memory, stats will be calculated from file from disk
    return dataLog


def logMemoryUsageToFileEveryHour(memLogPath, dataLogPath, dataLog_last60seconds, dataLog_last60minutes, counter):
    if counter % 3600 == 0:  #should be 3600
        if not os.path.exists(memLogPath):
            with open(memLogPath, 'w') as m:
                m.write("Hello at: %s \n" % datetime.datetime.now())
        with open(memLogPath, 'a') as memLog:
            uptime_minutes = counter / 60
            uptime_hours = round(uptime_minutes/60, 1)
            uptime_days = round(uptime_hours / 24, 1)

            logEntry = f"List sizes: dataLog_last60seconds:{len(dataLog_last60seconds)}; " + \
                f"_last60minutes:{len(dataLog_last60minutes)}; " + \
                f"file_Byte:{os.path.getsize(dataLogPath)}; " + \
                f"⏰Counter:{counter} " + \
                f"Uptime minutes:{uptime_minutes}=" + \
                f"hours:{uptime_hours}=" + \
                f"days:{uptime_days}"

            logging.info(f"__MEM: {logEntry}")
            memLog.write(f"{logEntry}\n")


def appendEverySecondAndDiscardEntriesOlderThan60seconds(dataLog_last60seconds, currentValue, counter):
    dataLog_last60seconds.append(float(currentValue))
    dataLog_last60seconds = _discardOldDataEntries(dataLog_last60seconds, 60)
    return dataLog_last60seconds


def _discardOldDataEntries(d, numberOfEntries):
    while len(d) > numberOfEntries:
        d.pop(0)  # remove oldest entry
    return d


if __name__ == "__main__":
    print("Use main.py to start application")
    sys.exit(0)




class UnitIsNotInMicroSievertException(Exception):
    pass


class AlarmTimeRangeException2(Exception):
    pass
