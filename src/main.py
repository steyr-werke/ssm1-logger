#!/usr/bin/env python3
"""
Send SMS with values from RS232
"""
import datetime
import logging
import os
import shutil
import sys
import time
import traceback

import config
import dataStore
import statsEngine
import ruleEngine
import sms
import ssm1Tests

projectRootPath = os.path.dirname(os.path.realpath(__file__))


# === CONFIGURE LOGGING ===
LOG_FILE = f"{projectRootPath}/log/application.log"

# === Enable Tests ==
ENABLE_TESTS = False
# === Mock SMS Interface, if true, print sms to stdout only ===
ENABLE_MOCK_SMS = False
# === Mock Serial Interface, use mock data instead of data from serial /dev/ttyUSB0 ===
ENABLE_MOCK_RS232 = False

### CONFIGURATION START
FALLBACK_ADMIN_PHONE = "+436502090815"  # hardcoded, if config can not be read.
SERIAL_PORT = "/dev/ttyUSB0"
SERIAL_BAUD = 9600
### CONFIGURATION END

CONFIG_PATH = f"{projectRootPath}/config/config.json"
DEFAULT_CONFIG_PATH = f"{projectRootPath}/config/default_config.json"
PERSISTENT_DATA_LOG_BY_MINUTE_PATH = f"{projectRootPath}/log/SSM1-Logger-dataLog.csv"
MEM_USAGE_LOG_PATH = f"{projectRootPath}/log/memory_usage.log"
SMS_INBOX_PATH = f"{projectRootPath}/../run/gammu-sms/inbox/"
SMS_OUTBOX_PATH = f"{projectRootPath}/../run/gammu-sms/outbox/"
LOG_SMS_INBOX_PATH = f"{projectRootPath}/log/LOG_SMS_INBOX.log"
LOG_SMS_OUTBOX_PATH = f"{projectRootPath}/log/LOG_SMS_OUTBOX.log"

### INTERNAL DATA STRUCTURES
dataLog_last60minutes = []
dataLog_last60seconds = []
currentValue = 0
currentDataString = ""
currentValue = 0
minuteMeanValue = 0


logging.basicConfig(
    format='%(levelname)s:%(asctime)s %(message)s',
    #level=logging.DEBUG,
    level=logging.INFO,
    handlers=[
        #logging.FileHandler(LOG_FILE, mode='a'),
        logging.StreamHandler() #stdout
        ]
    )
logging.info("Initialized logging...")


def main():
    """
    Strategy:
        1. Get value every second from RS232
        2. Create a list of last minute
        3. Calculate last_minute_median_value and write it to dataLog
        4. If last_minute_median_value > user level, then send sms to user (check for each user)
        5. Calculate last_hour_median_value, last_day_median_value and last_week_median_value from saved csv file and append it to sms
    
    """
    counter = 0  # Assuming 1 up is one second
    adminAlertCounter = 0
    adminAlertTimeout = 3600  # one hour between admin fatal error notifications
    config.resetUserCounters()
    backupAndCleanupLogs()

    while(True):
        # check for any user commands
        sms.processIncomingSmsCommandFromFileDaemon(minuteMeanValue, currentValue, currentDataString)

        try:
            counter = loopData(counter)
            adminAlertCounter = 0  # every exception should be logged asap

        except KeyboardInterrupt:
            logging.info("Application quit with CTRL-c. Bye.")
            sys.exit(0)

        except Exception as e:
            logging.error("🔥[ERROR] Exception raised: %s" % e)
            traceback.print_exc()
            counter = counter + 1 # increase counter as in exception counterUp() is not processed
            time.sleep(1) # force sleep as no serial interface is available to control the loop time

            if adminAlertCounter == 0:
                sms.logAndSmsToAdmins(f"Fatal error:\n{currentDataString}\n{e}")
                adminAlertCounter = counter
                
            elif adminAlertCounter + adminAlertTimeout < counter:
                sms.logAndSmsToAdmins(f"Still fatal error:\n{currentDataString}\n{e}")
                adminAlertCounter = counter


def loopData(counter):
    """
    Loop should be every second, according to SMM1 RS232 data input
        - check median values (every minute)
        - check sms rules (every minute)
        - persist data to file (every hour)
    """
    global dataLog_last60seconds
    global dataLog_last60minutes
    global currentDataString
    global currentValue
    global minuteMeanValue

    # 1. Get data
    try:
        if ENABLE_MOCK_RS232: #mock with unexpected data
            if ENABLE_TESTS: #mock with unexpected data
                currentDataString = dataStore.getDataString_mockUnexpectedData(counter)
            else:
                currentDataString = dataStore.getDataString_mock_expected_data()
        else:
            currentDataString = dataStore.getDataEntry()

        currentValue = dataStore.interpretDataString(currentDataString)

    except ValueError as UnitIsNotInMicroSievertException:  # set currentValue incredibly high, to make sure every user will be notified
        currentValue = 99999999999
        logging.error( f" Error: unit is not in Sievert. Setting current value incredibly high: {currentValue}")
        raise

    # 2. Append current value to dataLog_last60seconds, for minute mean analysis
    dataLog_last60seconds = dataStore.appendEverySecondAndDiscardEntriesOlderThan60seconds(
        dataLog_last60seconds,
        currentValue,
        counter
    )

    # 3. Calculate average value over last minute
    minuteStats = statsEngine.calculateStats(dataLog_last60seconds)
    minuteMeanValue = statsEngine.calculateMean(dataLog_last60seconds)

    # 4. If minuteMeanValue is too high, send message with stats to all users
    ruleEngine.checkAlarmUserRules(minuteMeanValue, currentValue, currentDataString, counter)


    # append minute median to dataLog every minute
    dataLog_last60minutes = dataStore.appendEveryMinute(
        dataLog_last60minutes,
        minuteMeanValue,
        counter
    )

    # publish minute median to mqtt
    dataStore.publishEveryMinute(
        dataLog_last60minutes,
        minuteStats,
        counter
    )

    dataStore.printLogEveryMinute(dataLog_last60minutes, minuteMeanValue, counter)

    # save dataLog to file every hour
    dataLog_last60minutes = dataStore.saveToDiskAndCleanupEveryHour(
        PERSISTENT_DATA_LOG_BY_MINUTE_PATH,
        dataLog_last60minutes,
        counter
    )

    dataStore.logMemoryUsageToFileEveryHour(
        MEM_USAGE_LOG_PATH,
        PERSISTENT_DATA_LOG_BY_MINUTE_PATH,
        dataLog_last60seconds,
        dataLog_last60minutes,
        counter
    )

    #logging.info(f"➤curr:{currentValue}; minute_mean:{minuteMeanValue}; currentDataString: {currentDataString}; minute_data:{dataLog_last60seconds}\n\n")
    logging.debug(f"➤curr:{currentValue}; minute_mean:{minuteMeanValue}; ")

    counter = _counterUp(counter)

    return counter


def _counterUp(counter):
    """Keep the memory and the integers in sane ranges."""
    # oneHourInSeconds = 60*60            # 3600sec/hour
    # oneDayInSeconds = 60*60*24          # 86400sec/day
    # oneWeekInSeconds = 60*60*24*7       # 604800sec/week
    oneYearInSeconds = 60*60*24*365     # 31536000sec/year
    
    # After 1 year, 
    #  - the dataLog size is about 30MB.
    #  - the memory log size is about 2MB.
    #  - the SMS log size is about 1,5MB.

    counter = counter + 1

    if counter > oneYearInSeconds:
        counter = 0
        config.resetUserCounters()
        backupAndCleanupLogs()
    return counter


def backupAndCleanupLogs():
    """Copy existing data into backup file, continue with a blank file."""
    _backupAndCleanupLogs(PERSISTENT_DATA_LOG_BY_MINUTE_PATH, True)
    _backupAndCleanupLogs(MEM_USAGE_LOG_PATH, True)
    _backupAndCleanupLogs(LOG_SMS_OUTBOX_PATH, True)
    _backupAndCleanupLogs(LOG_SMS_INBOX_PATH, True)
    _backupAndCleanupLogs(CONFIG_PATH, False)


def _backupAndCleanupLogs(filePath, cleanupLogFile: bool):
    """Backup existing log files with date suffix."""
    perfectDate = datetime.datetime.now().date().isoformat()

    if os.path.exists(filePath): #we have an existing logfile, so create a numbered backup
        for i in range(0, 65000): # a number appended to the copied file name like dataLog_4.log
            backupPath_memLog = f"{filePath}_{perfectDate}_{i}.log"

            if not os.path.exists(backupPath_memLog): #backup to new file
                shutil.copyfile(filePath, backupPath_memLog)
                break

    if cleanupLogFile:
        with open(filePath, 'w') as f: #initialize empty file
            f.write("")


def getCurrentTimestamp():
    date_isoformat = datetime.datetime.now().date().isoformat()
    time_isoformat = datetime.datetime.now().time().isoformat()
    #return f"{date_isoformat};{time_isoformat}"
    return (date_isoformat, time_isoformat)


if __name__ == "__main__":
    if ENABLE_TESTS:
        ssm1Tests.runTests()
    else:
        main()

    sys.exit(0)






class UnitIsNotInMicroSievertException(Exception):
    pass
