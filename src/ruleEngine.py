#!/usr/bin/env python3
"""
Apply user rules
"""
import logging
import os
import sys
import time
import statistics

import sms
import config
import main


def checkAlarmUserRules(minuteMeanValue, currentValue, currentDataString, counter):
    """ if minuteMeanValue is too high, send message with stats to each user"""
    cfg = config.getConfig()

    for user in cfg['users']:
        if _userAlarmValueExceeded(minuteMeanValue, user): #TODO FIX wrong: function name should be lower than
            # logging.debug(f"_userAlarmValueExceeded {user['phone']}: {minuteMeanValue}>{user['alarmValue']}; Check if user {user['phone']} has been alarmed after: {counter} UserNotifiedAt:{user['notifiedAt']}")

            if _isUserNotificationTimeoutReached(user, counter):
                logging.info(f"_isUserNotificationTimeoutReached: {user['phone']}: {counter} exceeds timeout: {user['notifiedAt']}")
                user['notifiedAt'] = counter  # update this user object to update in the cfg object to be persistable to json

                sms.sendAlertSmsTo(user['phone'], user['alarmValue'], minuteMeanValue, currentValue, currentDataString)

                config.setConfig(cfg) # do not persist on every cycle, only after updated notifiedAt timestamp


def _userAlarmValueExceeded(minuteMeanValue, user):
    if minuteMeanValue > float(user['alarmValue']):
        return True
    return False


def _isUserNotificationTimeoutReached(user, counter):
    if counter > user['notifiedAt'] + user['alarmInterval_s']: #notify user again
        #logging.info(f"Counter: {counter}, userNotifiedAtCounterValue:{user['notifiedAt']}")
        return True
    elif user['notifiedAt'] == 0: # user has never been notified, so the first alarm should trigger a notification
        return True
    return False


if __name__ == "__main__":
    print("Use main.py to start application")
