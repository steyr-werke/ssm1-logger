#!/usr/bin/env python3
"""
Get and set stored configs
"""
import logging
import os
import sys
import time
import json
import re

import main
import sms

USERS_ROLES = [
    'user',
    'admin',
    'watchdog'
]


def getConfig():
    return _readConfigFromFileAsDict()

def setConfig(config):
    return _writeConfigToFileAsDict(config)


def resetUserCounters():
    config = _readConfigFromFileAsDict()
    for user in config['users']:
        user['notifiedAt'] = 0
    _writeConfigToFileAsDict(config)


def getWatchdogPhone():
    """Return phone number of watchdog"""
    config = _readConfigFromFileAsDict()
    return config['watchDog']['phone']


def getUserRole(userPhone):
    cfg = _readConfigFromFileAsDict()
    for user in cfg['users']:
        if user['phone'] == userPhone:
            return user['role']
    return ""


def setUserRole(userPhone, newRole):
    cfg = _readConfigFromFileAsDict()

    if _isUser(userPhone, cfg):
        # check if newRole is admin or user
        if newRole.lower() not in USERS_ROLES:
            raise ValueError(f"Unknown role to set.")

        for user in cfg['users']:
            if user['phone'] == userPhone:
                user['role'] = newRole.lower()

        _writeConfigToFileAsDict(cfg)
        return True

    else:
        sms.logAndSmsToAdmins("UserNotFound: setUserRole failed. User %s not found." % userPhone)
        return False


def setAlarmValue(userPhone: str, alarmValue: str):
    """Expect alarmValue as string, make comma point and return float."""
    cfg = _readConfigFromFileAsDict()

    if _isUser(userPhone, cfg):
        try:
            alarmValue = _validateAlarmValueStrToFloat(alarmValue)
        except:
            sms.sendSmsToRecipient(userPhone, "Setting alarm value to %s failed. \n Example: 0.3" % alarmValue)
            return False

        for user in cfg['users']:
            if user['phone'] == userPhone:
                user['alarmValue'] = float(alarmValue)

        _writeConfigToFileAsDict(cfg)
        return True

    else:
        sms.logAndSmsToAdmins("WARN: setAlarmValue failed. User %s not found." % userPhone)
        return False


def getAlarmValue(userPhone):
    cfg = _readConfigFromFileAsDict()
    for user in cfg['users']:
        if user['phone'] == userPhone:
            return user['alarmValue']
    return ""


def setAlarmInterval_minutes(userPhone:str, alarmInterval_m:str):
    try:
        alarmInterval_s = int(alarmInterval_m) * 60
    except ValueError as ve:
        sms.sendSmsToRecipient(userPhone, "ValueError: Setting alarm interval to %s failed. \n Use time in minutes like: 1, 60, 1440" % alarmInterval_m)
        return False

    return setAlarmInterval_seconds(userPhone, alarmInterval_s)

def getAlarmInterval_minutes(userPhone:str):
    ret = getAlarmInterval_seconds(userPhone)
    if type(ret) == int:
        return ret / 60
    else:
        return ret


def setAlarmInterval_seconds(userPhone, alarmInterval_s):
    cfg = _readConfigFromFileAsDict()

    if _isUser(userPhone, cfg):
        try:
            alarmInterval_s = _validateAlarmInterval(alarmInterval_s)
        except:
            sms.sendSmsToRecipient(userPhone, "Setting alarm interval to %s failed. \n Usage examples: 1, 60, 1440" % alarmInterval_s)
            return False

        for user in cfg['users']:
            if user['phone'] == userPhone:
                user['alarmInterval_s'] = alarmInterval_s  #TODO use safe implementation from setAlarmValue above

        _writeConfigToFileAsDict(cfg)
        return True

    else:
        sms.logAndSmsToAdmins("UserNotFound: setAlarmInterval failed. User %s not found." % userPhone)
        return False


def getAlarmInterval_seconds(userPhone):
    cfg = _readConfigFromFileAsDict()
    for user in cfg['users']:
        if user['phone'] == userPhone:
            return user['alarmInterval_s']
    return ""



# def setAlarmTime(userPhone, timeOrOff):
#     userFound = False
#     config = _readConfigFromFileAsDict()
#     for user in config['users']:
#         if user['phone'] == userPhone:
#             userFound = True

#     if userFound:
#         try:
#             timeOrOff = _validateTimeOrOff(timeOrOff)
#         except AttributeError as ax:
#             sms.sendSmsToRecipient(userPhone, "Setting alarm time failed: %s \n Usage examples: 3:20, 18:00 or off" % timeOrOff)
#             return False

#         user['alarmTime'] = timeOrOff
#         _writeConfigToFileAsDict(config)
#         return True

#     else:
#         sms.logAndSmsToAdmins("WARN: setAlarmTime failed. User %s not found." % userPhone)
#         return False


# def getAlarmTime(userPhone):
#     config = _readConfigFromFileAsDict()
#     for user in config['users']:
#         if user['phone'] == userPhone:
#             return user['alarmTime']
#     return ""


def getUserPhone(userPhone):
    config = _readConfigFromFileAsDict()
    for user in config['users']:
        if user['phone'] == userPhone:
            return user['phone']
    return ""


def getUsersPermissive(adminPhone):
    if _isAdmin(adminPhone): #TODO remove check, as security should only be on sms level
        config = _readConfigFromFileAsDict()
        return config['users']
    return ""

def getAllUsersAndRolesAsString(adminPhone:str):
    if _isAdmin(adminPhone): #TODO remove check, as security should only be on sms level
        config = _readConfigFromFileAsDict()
        entries = ""
        for entry in config['users']:
            entries = f"{entry['phone']} ({entry['role']})\n {entries}"

        return entries
    else:
        raise AdminDoesNotExistException(f"Request all users not allowed. Requestor: {adminPhone}")

def getSingleUserConfigAsString(userPhone):
    config = _readConfigFromFileAsDict()
    for user in config['users']:
        if user['phone'] == userPhone:
            return str(user).replace(',', '\n').replace('{', '').replace('}', '').replace('\'', '')


def addUser(userPhone:str, adminPhone:str):
    if _isAdmin(adminPhone): #TODO remove check, as security should only be on sms level
        try:
            userPhone_asPlusNumber = _validateUserPhoneToPlusNumber(userPhone)
        except Exception as ex:
            sms.sendSmsToRecipient(adminPhone, f"Phone number validation failed: {userPhone}\n{ex}")
            return False

        logging.info("Adding %s" % userPhone)
        config = _readConfigFromFileAsDict()
        for user in config['users']:  # test if user already exists
            if user['phone'] == userPhone:
                sms.logAndSmsToAdmins("User add %s failed. User already exists: %s" % (userPhone, user))
                return False

        user = { "phone": userPhone_asPlusNumber, "role": "user", "alarmInterval_s": 3600, "alarmValue": 0.4, "notifiedAt": 0 }
        config['users'].append(user)
        _writeConfigToFileAsDict(config)
        return True
    else:
        raise AdminDoesNotExistException("User add %s failed. Forbidden for user %s" % (userPhone, adminPhone))


def _validateUserPhoneToPlusNumber(userPhone: str):
    """Read userphone as 0043 or +43 and change to +43 number."""
    phone_number_min_length = 10
    phone_number_max_length = 15
    userPhone_onlyNumber = ""

    userPhone = userPhone.strip().replace(" ", "")

    # Get only the number out of userPhone, e.g. 43650123123 or 0043650123123
    # and remove invisible bounding chars when copied phone number from Signal App contact
    try:
        mo1 = re.search("(\d+)", userPhone)
        userPhone_onlyNumber = mo1.group(1)
    except:
        raise ValueError(f"User phone not a number: {userPhone}")

    if userPhone_onlyNumber.startswith("00"):
        userPhone_onlyNumber = userPhone_onlyNumber[2:]  #remove preceeding 00

    userPhone_onlyNumber = f"+{userPhone_onlyNumber}"

    # Check length of phone number https://de.wikipedia.org/wiki/Rufnummer#L%C3%A4nge_von_Rufnummern
    if len(userPhone_onlyNumber) > phone_number_max_length or len(userPhone_onlyNumber) < phone_number_min_length:
        raise ValueError(f"User phone has unusual length: {userPhone}")

    return userPhone_onlyNumber


def deleteUser(userPhone:str, adminPhone:str):
    if _isAdmin(adminPhone): #TODO remove check, as security should only be on sms level
        logging.info("Deleting user %s (requested by %s)" % (userPhone, adminPhone))
        try:
            userPhone_asPlusNumber = _validateUserPhoneToPlusNumber(userPhone)
        except Exception as ex:
            sms.sendSmsToRecipient(adminPhone, f"Phone number validation failed: {userPhone}\n{ex}")
            return False

        config = _readConfigFromFileAsDict()
        userFound = False
        for user in config['users']:
            if user['phone'] == userPhone_asPlusNumber:
                config['users'].remove(user)
                userFound = True

        if not userFound:
            sms.sendSmsToRecipient(adminPhone, f"User phone number not found: {userPhone}")
            return False

        _writeConfigToFileAsDict(config)

        return True
    else:
        raise AdminDoesNotExistException("User deletion %s failed. Forbidden for user %s" % (userPhone, adminPhone))


def _writeConfigToFileAsDict(config):
    """Write config to file"""
    with open(main.CONFIG_PATH, 'w') as c:
        c.write(json.dumps(config, indent=4))


def _readConfigFromFileAsDict():
    """Open config.json file and return as dict. Else copy default.config first."""
    try:
        with open(main.CONFIG_PATH, 'r') as f:
            return json.load(f)
    except Exception as ex: #TODO is this safe? If concurrent access to config, currently, the existing config will be purged.
        logging.error("ERROR: Config could not be loaded. Copy from default_config.json. \n %s" % ex)
        with open(main.DEFAULT_CONFIG_PATH, 'r') as default_config:
            with open(main.CONFIG_PATH, 'w') as config:
                config.write(default_config.read())
        with open(main.CONFIG_PATH, 'r') as c:
            return json.load(c)


def getAdminPhones():
    """Return a list of admin phone numbers"""
    phones = [ main.FALLBACK_ADMIN_PHONE ]
    config = _readConfigFromFileAsDict()

    for user in config['users']:
        if user['role'] == "admin":
            phones.append(user['phone'])

    # remove duplicates i.e. fallback and admin phone
    return list(set(phones))


def getAllUserPhones():
    """Return a list of user and admin phone numbers"""
    phones = [ main.FALLBACK_ADMIN_PHONE ]
    config = _readConfigFromFileAsDict()

    for user in config['users']:
        phones.append(user['phone'])

    return phones


def _isAdmin(phoneNr):
    retVal = False
    for adminPhoneNr in getAdminPhones():
        if adminPhoneNr == phoneNr:
            retVal = True
    return retVal


def isKnownUser(phoneNr):
    """Return True if user exists either as user or as admin"""
    retVal = False
    for userPhoneNr in getAllUserPhones():
        if userPhoneNr == phoneNr:
            retVal = True
    return retVal


def _isUser(userPhone:str, cfg):
    """Return True if user exists"""
    for user in cfg['users']:
        if user['phone'] == userPhone:
            return True
    return False


def isUserInConfig(userPhone:str):
    """Return True if user exists"""
    cfg = _readConfigFromFileAsDict()
    for user in cfg['users']:
        if user['phone'] == userPhone:
            return True
    return False


# def _validateTimeOrOff(timeOrOff):
#     """Validate pattern. Allowed is off|Off|OFF|9:32|23:59"""
#     if timeOrOff.lower() == "off" or timeOrOff == "":
#         return timeOrOff.lower()
#         #TODO do not return anything that is not expected
#     else:
#         mo1 = re.search(r"^(\d{1,2}):(\d{1,2})$", timeOrOff)
#         if int(mo1.group(1)) > 23:
#             raise AlarmTimeRangeException("hour too high")
#         if int(mo1.group(2)) > 59:
#             raise AlarmTimeRangeException("minutes too high")

#         return timeOrOff


def _validateAlarmValueStrToFloat(alarmValue_str: str):
    """Expect alarmValue as string, make comma point and return float."""
    if not type(alarmValue_str) == str:
        raise AlarmValuePatternException("AlarmValue is not a valid input")

    return float(str(alarmValue_str).replace(",", "."))

def _validateAlarmInterval(val):
    return int(val)


if __name__ == "__main__":
    print("Use main.py to start application")
    sys.exit(0)



class AlarmTimeRangeException(Exception):
    pass

class AlarmValuePatternException(Exception):
    pass

class UserDoesNotExistException(Exception):
    pass

class AdminDoesNotExistException(Exception):
    pass