#!/bin/bash
#
# Frontend controller script for service loading
#
serviceName="SSM1-Logger"

scriptPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
logger "$serviceName script root path is: $scriptPath"
projectRootPath="/opt/SSM1-Logger"


if [ "$1" == "start" ]; then
    echo "Starting $serviceName ..."
    python -u ${projectRootPath}/src/main.py &
    echo $! > ${projectRootPath}/run/.application-${serviceName}.pid
    echo "Started ${projectRootPath} in background with pid `cat ${projectRootPath}/run/.application-${serviceName}.pid`"

elif [ "$1" == "stopkill" ]; then
    echo "Stopping $serviceName ..."
    #handled by systemd

elif [ "$1" == "stop" ]; then
    #if invoked via run_app.sh without systemd, we need to kill manually. 
    echo "Kill process by pid `cat ${projectRootPath}/run/.application-${serviceName}.pid`"
    kill `cat ${projectRootPath}/run/.application-${serviceName}.pid`

else
    echo "Usage: please provide one argument: [start|stop]"
fi
