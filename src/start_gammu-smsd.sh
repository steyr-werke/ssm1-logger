#!/bin/bash
#
# Frontend controller script for service loading
#
serviceName="gammu-smsd"

# Check if user is root
if [ `id -u` == 0 ]; then
    echo "You are root, ok."
else
    echo "You are not root. Quitting."
    exit 1
fi

scriptPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
logger "$serviceName script root path is: $scriptPath"
applicationPath="${scriptPath}/main.py"

rsync ${scriptPath}/../deploy/home_pi_.gammurc /etc/gammurc  # copy for root user
rsync ${scriptPath}/../deploy/home_pi_.gammurc /etc/gammu-smsdrc  # copy for root user

if [ "$1" == "start" ]; then
    echo "Waiting for modem..."
    python -u ${scriptPath}/wait_for_modem.py # > /tmp/wait_for_modem.log
    echo "Starting $serviceName ..."
    #exit 1  # exit for debugging purposes only
    gammu-smsd  &
    echo $! > ${scriptPath}/run/.application-${serviceName}.pid
    echo "Started ${serviceName} in background with pid `cat ${scriptPath}/run/.application-${serviceName}.pid`"

elif [ "$1" == "kill" ]; then
    echo "Stopping $serviceName ..."
    #handled by systemd

elif [ "$1" == "stop" ]; then
    #if invoked via run_app.sh without systemd, we need to kill manually. 
    echo "Kill process by pid `cat ${scriptPath}/run/.application-${serviceName}.pid`"
    kill `cat ${scriptPath}/run/.application-${serviceName}.pid`

###
### Helper functions for debugging
###
elif [ "$1" == "testsms" ]; then
    echo "Send test SMS to admin..."
    gammu --sendsms TEXT 00436502090815 -text "yey_it_works"

elif [ "$1" == "setsmsc" ]; then
    echo "setsmsc..."
    gammu setsmsc 1 "+436640501"

elif [ "$1" == "entersecuritycode" ]; then
    echo "entersecuritycode..."
    gammu entersecuritycode PIN 1124

elif [ "$1" == "getconfig" ]; then
    echo "Get SMSC code..."
    gammu getsmsc 1
    echo "Get security todos..."
    gammu getsecuritystatus

elif [ "$1" == "getusb" ]; then
    echo "Get serial USB devices using ls /dev/serial/by-id ..."
    ls -l /dev/serial/by-id/

elif [ "$1" == "getstatus" ]; then
    echo "List running gammu processes..."
    ps ax | grep gammu
    ls -l ${scriptPath}/../run/gammu-sms/inbox
    ls -l ${scriptPath}/../run/gammu-sms/outbox

else
    echo "Usage: please provide one argument: [start|stop|testsms|setsmsc|entersecuritycode|getconfig|getusb|getstatus]"
fi
