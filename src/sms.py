#!/usr/bin/env python3
"""
Sms component
"""
import logging
import os
import re

import config
import statsEngine
import main
import smsApi


THE_HELP_TEXT = """SSM1-Logger Befehle (case-insensitive):
# User:
W oder getValue
S oder setAlarmValue 0.4   [microSievert]
I oder setAlarmInterval 60   [minutes]
H oder help

# Admin: (no spaces in phone numbers)
addUser +4312345678
deleteUser +4312345678
setRole +4312345678 admin
getUsers
"""

THE_WELCOME_MESSAGE = """Willkommen! Deine Telefonnummer wurde als SMS Empfänger für die Strahlungswerte in Steyr hinzugefügt.
Antworte mit 'help' für weitere Infos.
Dein SSM1-Logger Team / RK Steyr
"""


def processIncomingSmsCommandFromFileDaemon(minuteMeanValue, currentValue, currentDataString):
    """Find latest sms file, get sms and phone, process command, delete file.""" 
    files = os.listdir(main.SMS_INBOX_PATH)

    if len(files) < 1: #no command received
        logging.debug("No incoming SMS cmd from file recieved: %d" % len(files))
        return

    firstFilePath = main.SMS_INBOX_PATH + files[0]  # process only one file per cycle

    try:
        requesterPhoneNr, msg = _loadSmsFromFile(firstFilePath)

        # Security check if user is registered
        # #where the phoneNr is read from config.
        # Raise if user is unknown.
        checkIncomingPhoneIsKnownUserPhone(requesterPhoneNr)

        requestCommand, arg2, arg3 = _splitIncomingCommand(msg)

        logging.info(f"Incoming command: {requestCommand} with arguments: {arg2} {arg3}")

        _logIncomingSms(requesterPhoneNr, requestCommand, arg2)

        #
        # Admin commands
        #
        if _isRegisteredAdminCommand(requestCommand):
            if not config._isAdmin(requesterPhoneNr):  # Security check
                raise LookupError("Non-admin requested admin command.")

            if requestCommand in _getListLower(smsApi.adminCommands['ADD_USER']):
                if config.addUser(arg2, requesterPhoneNr):
                    sendSmsToRecipient(requesterPhoneNr, f"User successfully added: {arg2}\nWelcome message will be sent.")
                    sendSmsToRecipient(arg2, THE_WELCOME_MESSAGE)

            elif requestCommand in _getListLower(smsApi.adminCommands['DELETE_USER']):
                if config.deleteUser(arg2, requesterPhoneNr):
                    sendSmsToRecipient(requesterPhoneNr, f"User successfully deleted: {arg2}")

            elif requestCommand in _getListLower(smsApi.adminCommands['SET_USER_ROLE']):
                config.setUserRole(userPhone=arg2, newRole=arg3)
                sendSmsToRecipient(requesterPhoneNr, f"User role updated to: {arg3}")

            elif requestCommand in _getListLower(smsApi.adminCommands['GET_USERS']):
                response = config.getAllUsersAndRolesAsString(requesterPhoneNr)
                sendSmsToRecipient(requesterPhoneNr, f"User List: \n{response}")

            elif requestCommand in _getListLower(smsApi.adminCommands['GET_USAGE']):
                response = _getIncomingSmsLogTail(numLines_str=arg2)
                sendSmsToRecipient(requesterPhoneNr, f"Latest inbox requests: \n{response}")

        #
        # User commands
        #
        elif _isRegisteredUserCommand(requestCommand):
            if not config.isKnownUser(requesterPhoneNr):  # Security check
                raise LookupError("Unknown user requested user command.")

            if requestCommand in _getListLower(smsApi.userCommands['GET_VALUE']):
                # val = f"Aktueller Wert: {currentDataString}"
                msg = createAlertSmsMessage(minuteMeanValue, currentValue, currentDataString)
                sendSmsToRecipient(requesterPhoneNr, msg)


            # elif requestCommand in _getListLower(smsApi.userCommands['GET_ALARM_VALUE']):
            #     alarmValue = config.getAlarmValue(requesterPhoneNr)
            #     sendSmsToRecipient(requesterPhoneNr, f"Alarm value is set to: {alarmValue}")

            elif requestCommand in _getListLower(smsApi.userCommands['SET_ALARM_VALUE']):
                if config.setAlarmValue(requesterPhoneNr, arg2):
                    sendSmsToRecipient(requesterPhoneNr, f"Alarm value updated to: {arg2}")

            # elif requestCommand in _getListLower(smsApi.userCommands['GET_ALARM_INTERVAL']):
            #     alarmInterval = config.getAlarmInterval_minutes(requesterPhoneNr)
            #     sendSmsToRecipient(requesterPhoneNr, f"Alarm interval is set to: {alarmInterval}")

            elif requestCommand in _getListLower(smsApi.userCommands['SET_ALARM_INTERVAL']):
                if config.setAlarmInterval_minutes(requesterPhoneNr, arg2):
                    sendSmsToRecipient(requesterPhoneNr, f"Alarm interval updated to: {arg2}")

            elif requestCommand in _getListLower(smsApi.userCommands['HELP']):
                msg = f"Aktueller Wert: {currentDataString}\n\n" \
                    f"Aktuelle Settings: \n{config.getSingleUserConfigAsString(requesterPhoneNr)}\n\n" \
                    f"{THE_HELP_TEXT}"
                sendSmsToRecipient(requesterPhoneNr, msg)

        else:
            logging.warning(f"Invalid command from user {requesterPhoneNr} SMS: {msg}")
            if config.isKnownUser(requesterPhoneNr):  #no responses to unknown users
                sendSmsToRecipient(requesterPhoneNr, f"Invalid command from user {requesterPhoneNr} \nSMS: {msg}")

    except Exception as e:
        logging.warning(f"Error processing incoming command from user {requesterPhoneNr} \nSMS: {msg} \n{e}")
        sendSmsToRecipient(main.FALLBACK_ADMIN_PHONE, f"Error processing incoming command from user {requesterPhoneNr} \nSMS: {msg} \n{e}")

    os.remove(firstFilePath)


def checkIncomingPhoneIsKnownUserPhone(phoneNr:str):
    """Raise exception if phoneNr is not registered."""
    if config.isUserInConfig(phoneNr):
        return True
    else:
        raise ValueError(f"Invalid user requested SMS: {phoneNr}")


def _getPhoneNrFromFilePath(filePath: str):
    mo1 = re.search('.*(\+43\d*)_.*', filePath)
    return mo1.group(1)


def _loadSmsFromFile(filePath: str):
    """Get phone number from filePath and SMS text from file content"""
    with open(filePath, 'r') as inFile:
        try:
            phoneNr = _getPhoneNrFromFilePath(filePath)
        except:
            phoneNr = main.FALLBACK_ADMIN_PHONE

        msg = inFile.read()
        #logging.info("INCOMING SMS: read SMS from file: phoneNr:", phoneNr, "; Message:", msg)
        logging.info("INCOMING SMS: read SMS from file: phoneNr: %s ; Message: %s" % (phoneNr, msg))
        return (phoneNr, msg)


def _splitIncomingCommand(msg:str):
    """
        Validate commands from file like: (2nd arg is optional)
        - addUser 1234567
        - help
    """
    logging.info("Incoming command: %s", msg)
    msg = msg.strip().lower()

    # split msg in command and value
    msgItems = msg.split(" ")
    command = msgItems[0].strip() #arg1
    arg2 = ""
    arg3 = ""

    if len(msgItems) >= 2: #if we have a 2nd arg, this is the value
        arg2 = msgItems[1].strip()

    if len(msgItems) >= 3: #3rd optional arg
        arg3 = msgItems[2].strip()
    
    return command, arg2, arg3


def _isRegisteredAdminCommand(command):
    for cmdGroup in smsApi.adminCommands.keys():
        for cmd in smsApi.adminCommands[cmdGroup]:
            if command.lower() == cmd.lower():
                return True
    return False


def _isRegisteredUserCommand(command):
    for cmdGroup in smsApi.userCommands.keys():
        for cmd in smsApi.userCommands[cmdGroup]:
            if command.lower() == cmd.lower():
                return True
    return False

#def _getSmsApiCommandAliases()
def _getListLower(aList):
    """Make predefined commands case insensitive"""
    o = []
    for i in aList:
        o.append(i.lower())
    return o

# def sendSmsToRecipientREAL(phoneNr:str, msg:str):
#     # cfg = "/home/pi/.gammurc"
#     # cmd = f"gammu --config {cfg} --sendsms TEXT {phoneNr} -text \"{msg}\""
#     # subprocess.call(cmd, shell=True)
#     # Do not use cmd call, as device is blocked by smsd for receiving sms


def sendSmsToRecipient(phoneNr, message):
    printMsg = str(message).replace("\n", "; ")
    if main.ENABLE_MOCK_SMS:
        lineSep = f"OUTGOING SMS: ⭐ MOCK SMS to {phoneNr}:"
        logging.info(f"{lineSep} {printMsg}")

    else:
        lineSep = f"OUTGOING SMS: ⭐ REAL SMS to {phoneNr}:"
        logging.info(f"{lineSep} {printMsg}")

        _writeSmsOutboxFileForGammuFileWatcher(phoneNr, message)

    _logOutgoingSms(phoneNr, printMsg)


def _writeSmsOutboxFileForGammuFileWatcher(phoneNr:str, msg:str):
    smsOutboxPath = f"{main.SMS_OUTBOX_PATH}OUTSMS_{phoneNr}_00.txt"
    
    # create new file if file already exists
    if not os.path.exists(smsOutboxPath):
        with open(smsOutboxPath, 'w') as outFile: #append to file to sms daemon watched folder
            outFile.write(str(msg))
    
    else:  #we have a still not processed sms file
        for i in range(1,5): #send max 5 sms
            smsOutboxPath = f"{main.SMS_OUTBOX_PATH}OUTSMS_{phoneNr}_0{i}.txt"

            if not os.path.exists(smsOutboxPath): 
                with open(smsOutboxPath, 'w') as outFile: #append to file to sms daemon watched folder
                    outFile.write(str(msg))
                break


def sendSmsToRecipiens(phoneNrs, message):
    for nr in phoneNrs:
        sendSmsToRecipient(nr, message)


def logAndSmsToAdmins(message):
    sendSmsToRecipiens(config.getAdminPhones(), message)
    #logging.info(message)


def createAlertSmsMessage(minuteMeanValue, currentValue, currentDataString):
    dataLogPath = main.PERSISTENT_DATA_LOG_BY_MINUTE_PATH
    hourValue = statsEngine.calculateMeanFromTo(dataLogPath, numDataEntriesToEvaluate=60, hint="hour")
    dayValue = statsEngine.calculateMeanFromTo(dataLogPath, numDataEntriesToEvaluate=60*24, hint="day")
    weekValue = statsEngine.calculateMeanFromTo(dataLogPath, numDataEntriesToEvaluate=60*24*7, hint="week")

    msg = f"Aktueller Wert: {currentValue}\n" + \
        f"Aktuelle Ausgabe: {currentDataString}\n" + \
        f"Letzte Minute (Mittelwert): {round(minuteMeanValue,3)} \n" + \
        f"Letzte Stunde: {round(hourValue, 3)}\n" + \
        f"Letzter Tag: {round(dayValue, 3)}\n" + \
        f"Letzte Woche: {round(weekValue, 3)}"

    return msg


def sendAlertSmsTo(phoneNr, alarmValue, minuteMeanValue, currentValue, currentDataString):
    dataLogPath = main.PERSISTENT_DATA_LOG_BY_MINUTE_PATH
    hourValue = statsEngine.calculateMeanFromTo(dataLogPath, numDataEntriesToEvaluate=60, hint="hour")
    dayValue = statsEngine.calculateMeanFromTo(dataLogPath, numDataEntriesToEvaluate=60*24, hint="day")
    weekValue = statsEngine.calculateMeanFromTo(dataLogPath, numDataEntriesToEvaluate=60*24*7, hint="week")

    msg = f"Aktueller Wert: {currentValue}\n" + \
        f"Aktuelle Ausgabe: {currentDataString}\n" + \
        f"Letzte Minute (Mittelwert): {round(minuteMeanValue,3)} (alarmValue:{alarmValue})\n" + \
        f"Letzte Stunde: {round(hourValue, 3)}\n" + \
        f"Letzter Tag: {round(dayValue, 3)}\n" + \
        f"Letzte Woche: {round(weekValue, 3)}"

    sendSmsToRecipient(phoneNr, msg)


def _logOutgoingSms(phoneNr:str, msg:str):
    with open(main.LOG_SMS_OUTBOX_PATH, 'a') as smsLog:
        date_isoformat, time_isoformat = main.getCurrentTimestamp()
        smsLog.write(f"SMS to;{phoneNr};{date_isoformat};{time_isoformat};{msg}\n")


def _logIncomingSms(phoneNr:str, command:str, value:str):
    with open(main.LOG_SMS_INBOX_PATH, 'a') as smsLog:
        date_isoformat, time_isoformat = main.getCurrentTimestamp()
        smsLog.write(f"SMS from;{phoneNr};{date_isoformat};{time_isoformat};{command};{value}\n")


def _getIncomingSmsLogTail(numLines_str="5"):
    if numLines_str == "":
        numLines_str = 5

    numLines = int(numLines_str)
    outStr = ""

    with open(main.LOG_SMS_INBOX_PATH, 'r') as smsLog:
        list = smsLog.readlines()

        if len(list) < numLines:
            outStr = _listToNewlineString(list, len(list))  #tail all we have
        else:
            outStr = _listToNewlineString(list, numLines)

    return outStr


def _listToNewlineString(list, numLines):
    fromIndex = len(list)-numLines
    toIndex = len(list)
    tail = list[fromIndex:toIndex]
    return str(tail).replace("[", "").replace("]", "").replace("'", "") 


if __name__ == "__main__":
    print("Use main.py to start application")
