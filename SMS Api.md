#SMS Api

Case insensitive!

## Manage Recipients (only allowed by admin tel nr)
"AddPhone: 012345678"
"DelPhone: 012345678"
"GetPhones"

## Manage Subscriptions Schedules (user settings)
setAlarmTime: 8:00|off
"GetSettings"


## Manage Subscription Data (user settings)
SetAlarmValue: 0.4|0,4
"GetValues" oder "Get"


## Help
"Help" oder "?"


## Set system time
setSystemTime: ??? (allows to set time via sms)

