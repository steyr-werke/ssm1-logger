#!/bin/bash
# Copy files to raspi using ssh


scriptBasePath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
fast=$1

#sshLogin="pi@10.0.0.129"
sshLogin="pi@10.0.0.114"

# test connection
ssh $sshLogin exit
if [ $? -ne 0 ]; then
    echo "$sshLogin not reachable. Quitting..."
    exit 1
fi

rsyncSource="."
deployBasePath="/opt/SSM1-Logger"
rsyncTarget="$sshLogin:$deployBasePath"
rsyncArgs="-e ssh -hari --delete-delay --inplace --exclude-from ${scriptBasePath}/_deploy-exclude-patterns.txt";

echo "=========================================================="
echo "😻  😻  😻  Deploying to ${sshLogin}  😻  😻  😻"
echo "=========================================================="
# kill remote service
ssh $sshLogin "sudo systemctl stop gammu-smsd.service"
ssh $sshLogin "sudo systemctl stop SSM1-Logger.service"

rsync $rsyncArgs ${rsyncSource} ${rsyncTarget}

echo "==================================================================="
echo "😻  😻  😻  Provisioning ${sshLogin}  😻  😻  😻"
echo "==================================================================="

#
# ASSURE SYSTEM DEPENDENCIES ARE INSTALLED ON THE HOST
#
ssh $sshLogin "/opt/SSM1-Logger/deploy/remote_1_install_system_dependencies.sh" $fast

#
# INSTALL AND RUN SERVICE
#
echo "Installing and running services..."
ssh $sshLogin "ls -l /dev/serial/by-id/"
ssh $sshLogin "/opt/SSM1-Logger/deploy/remote_2_install_and_run_service.sh" gammu-smsd root root
ssh $sshLogin "/opt/SSM1-Logger/deploy/remote_2_install_and_run_service.sh" SSM1-Logger pi pi

retVal=$?
if [ $retVal -eq 0 ]; then
	echo "Service successfully started. 😎"
else
	echo "Service could not be started. 💩💩💩"
fi

ssh $sshLogin "pgrep -a python"

sleep 1
echo "🤘 Syslog 🤘"
ssh $sshLogin "tail -n 15 /var/log/syslog"

sleep 1
echo "🤘 Application Log at ${deployBasePath} 🤘"
ssh $sshLogin "tail -f ${deployBasePath}/src/log/application.log"

exit 0
