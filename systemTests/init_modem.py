
import time
import re
import sys
import os
import gammu
import configparser

config = configparser.RawConfigParser()
config.read('/etc/gammu-smsdrc')

simPIN = config.get('smsd', 'PIN')
print("PIN",simPIN)
smscNumber =   config.get('smsd', 'SMSC')
print("SMSC",smscNumber)

def initModem():
  # Create state machine object
  sm = gammu.StateMachine()

  # Read config file
  sm.ReadConfig(Section=0,Filename="/etc/gammurc")
  #sm.ReadConfig(Section=1,Filename="/etc/gammurc")

  #conf0 = sm.GetConfig(0)
  #print('config 0:')
  #print(conf0)

  #conf1 = sm.GetConfig(1)
  #print('config 1:')
  #print(conf)

  # Connect to phone
  sm.Init()

  sec = sm.GetSecurityStatus()
  print("pre-PIN: security status", sec)

  if sec == "PIN": sm.EnterSecurityCode("PIN",simPIN)

  sec = sm.GetSecurityStatus()
  print("post-PIN: security status", sec)
  time.sleep(1)

  print("getting SMSC")
  attempts = 100
  for attempt in range(attempts):
    print("attempt",attempt)
    try:
      #sm.SetSMSC(smsc)
      smsc = sm.GetSMSC()
      print("getSMSC succes. Number %s" % smsc["Number"])
      break
    except:
      print("getSMSC failure")
    time.sleep(1)

  time.sleep(1)

  smsc["Number"] = smscNumber
  sm.SetSMSC(smsc)
  smsc = sm.GetSMSC()
  print('smsc Number: %s' % smsc["Number"])
  time.sleep(2)
  sys.exit(0)


initModem()
