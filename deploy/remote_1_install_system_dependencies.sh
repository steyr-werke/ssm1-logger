#!/usr/bin/env bash
#
# Run with deploy.sh. This script is run with every deployment on the remote host.
#
scriptPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
fast=$1

#
# INSTALL SYSTEM DEPENDENCIES
#
if [[ $fast != "fast" ]]; then #fast does not upgrade packages
    echo "Apt: update, upgrade and install? [y|N]"
    read processPackageManager
fi
if [[ $processPackageManager == "Y" || $processPackageManager == "y" ]]; then
    sudo apt-get update
    #sudo apt-get upgrade  # do not upgrade from remote ssh script because interactive things wont work

    echo "###"
    echo "### INSTALLING PACKAGES"
    echo "###"
    sudo apt -y install vim byobu rsync dnsutils
    #sudo apt -y install avahi-daemon avahi-utils
    sudo apt -y install python3 python3-pip
    #sudo pip3 install paho-mqtt

    ### ROOT LEVEL
    echo "### Install gammu SMS package"
    sudo apt -y install gammu
    sudo apt -y install libgammu-dev
    sudo apt -y install gammu-smsd
    echo "### Install python serial RS232 library globally"
    sudo pip install pyserial

    echo "### Install MQTT"
    sudo apt -y install mosquitto mosquitto-clients
    sudo rsync ${scriptPath}/../deploy/etc_mosquitto_mosquitto.conf /etc/mosquitto/mosquitto.conf
    sudo pip3 install paho-mqtt

    ### USER LEVEL
    #pip install python-gammu  # gammu command line tools, currently not in use


fi

exit 0

