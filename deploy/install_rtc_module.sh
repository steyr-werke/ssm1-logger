# device is RTC PI Real-Time Clock V3.0 from abel electronics
# see:     https://www.abelectronics.co.uk/kb/article/30/rtc-pi-setup-on-raspberry-pi-os

# Manual pepreparation:
# ---------------------
#  sudo raspi-config -> interface options ->   I5: i2C -> enable ? yes

# Install sofware
# ---------------

sudo apt-get update
sudo apt-get install python3-smbus python3-dev i2c-tools

# Attach following lines to /boot/config.txt
sudo sh -c 'cat >> /boot/config.txt <<EOF
# Changes for RTC PI module
dtparam=i2c_baudrate=100000  # correct speed for RTC PI
core_freq=250
dtoverlay=i2c-rtc,ds1307
EOF'

# Attach following lines to /etc/modules
sudo sh -c 'cat >> /etc/modules <<EOF
# Changes for RTC PI module
rtc-ds1307
EOF'

sudo nano /lib/udev/hwclock-set
#    comment out:
        #if [ -e /run/systemd/system ] ; then
        #exit 0
        #fi

sudo reboot

# after reboot:check or set time
#    date
#    sudo date -s "2 OCT 2015 18:00:00"
# write time to  to RTC !!!!!!!!!!
#    sudo hwclock -w
