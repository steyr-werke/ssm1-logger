#!/bin/bash

scriptPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
applicationBasePath="/opt/SSM1-Logger"

serviceName=$1
serviceUser=$2
serviceGroup=$3
echo "==================================================================="
echo "😻  😻  😻  Installing and starting service ${serviceName}  😻  😻  😻"
echo "==================================================================="
applicationServiceCommand="${applicationBasePath}/src/start_${serviceName}.sh"

echo "Creating ${serviceName} service file..."
mkdir ${applicationBasePath}/run
serviceFile=${applicationBasePath}/run/systemd-${serviceName}.service
cat > ${serviceFile} <<EOF
[Unit]
Description=${serviceName} start agent
After=syslog.target network-online.target

[Service]
Type=forking
User=${serviceUser}
#Group=${serviceGroup}
ExecStart=${applicationServiceCommand} start
ExecStop=${applicationServiceCommand} stop

[Install]
#WantedBy=multi-user.target
WantedBy=default.target
#WantedBy=network-online.target
EOF

echo "Installing service ${serviceName}.service..."
#sudo systemctl stop ${serviceName}.service

sudo chown root:root $serviceFile

# move service file to systemd folder
### BUG: sudo: no tty present and no askpass program specified --on hofb.at
sudo mv -vf $serviceFile /etc/systemd/system/${serviceName}.service
sudo systemctl daemon-reload
sudo systemctl enable ${serviceName}.service

sudo systemctl start ${serviceName}.service

exit $?
